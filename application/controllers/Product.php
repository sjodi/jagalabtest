<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_model');	
        $this->load->library('upload');
        
    }

	public function index()
	{
        $data = array(
            'title'     => 'Daftar Product',
            'subtitle'  => 'CRUD Manajemen data product',
            'category'  => $this->product_model->get_category()
        );
		$this->load->view('product_view',$data);
	}
    
    public function read_product()
    {
        echo json_encode($this->product_model->list_product());
    }
    
    public function add_product()
    {
        $valid = $this->form_validation;
        
        $config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'jpg|png';
		$config['max_size']	= '100000';
        
        $this->upload->initialize($config);
        
        if ($this->upload->do_upload('image')){
            $image     = $this->upload->data();
            $imageName = $image['file_name'];
        } else {
            $imageName = '';    
        }
        
        $code        = $this->input->post('code');
        $category    = $this->input->post('category');
        $name        = $this->input->post('name');
        $description = $this->input->post('description');
        $date        = $this->input->post('date');
        
        $valid->set_rules('code','Code','required');
        $valid->set_rules('category','Category','required');
        $valid->set_rules('name','Name','required');
        
        $data = array(
            'code'          => $code,
            'category'      => $category,
            'name'          => $name,
            'description'   => $description,
            'date'          => $date,
            'image'         => $image['file_name'],
        );
        
        if ($valid->run()) :
            $sukses = $this->product_model->insert_product($data);
        endif;
        
        if ($sukses) :
            $message = 'Berhasil tambah data produk';
        else :
            $message = 'Gagal tambah data produk';
        endif;
        
        $arr = array(
            'message' => $message,
            'image'   => $image
        );

        echo json_encode($arr);
        
    }
    
    public function update_product()
    {
        $valid = $this->form_validation;
        
        $config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'jpg|png';
		$config['max_size']	= '100000';
        
        $this->upload->initialize($config);
        
        if ($this->upload->do_upload('image')){
            $image     = $this->upload->data();
            $imageName = $image['file_name'];
        } else {
            $imageName = '';
        }
        
        $id          = $this->input->post('id');
        $code        = $this->input->post('code');
        $category    = $this->input->post('category');
        $name        = $this->input->post('name');
        $description = $this->input->post('description');
        $date        = $this->input->post('date');
        
        $valid->set_rules('code','Code','required');
        $valid->set_rules('category','Category','required');
        $valid->set_rules('name','Name','required');
        
        $data = array(
            'id'            => $id,
            'code'          => $code,
            'category'      => $category,
            'name'          => $name,
            'description'   => $description,
            'date'          => $date,
            'image'         => $imageName,
        );
        
        if ($valid->run()) :
            $sukses = $this->product_model->update_product($data);
        endif;
        
        if ($sukses) :
            $message = 'Berhasil edit data product';
        else :
            $message = 'Gagal edit data product';
        endif;
        
        $arr = array(
            'message' => $message,
            'date'    => $date,
        );

        echo json_encode($arr);
        
    }
    
    public function delete_product()
    {
        $id = $this->input->post('id');
        
        $sukses = $this->product_model->delete_product($id);
        
        if ($sukses) :
            $message = 'Berhasil hapus data product';
        else :
            $message = 'Gagal hapus data product';
        endif;
        
        $arr = array(
            'message' => $message
        );
        
         echo json_encode($arr);
    }
}
