<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_model');
    }

	public function index()
	{
        $data =  array(
            'title'         => 'Daftar Category',
            'subtitle'      => 'CRUD Manajemen data category',
            'cat'           => $this->category_model->list_category(),
        );
        
		$this->load->view('category_view',$data);
	}
    
    public function read_category()
    {
        echo json_encode($this->category_model->list_category());
    }
    
    public function add_category()
    {
        $valid = $this->form_validation;
        $name  = $this->input->post('name');
   
        $valid->set_rules('name','Nama Cat','required');
        
        $data  = array(
            'name' => $name,
        );
        
        if ($valid->run()) :
            $sukses = $this->category_model->insert_category($data);
        endif;
        
        if ($sukses) :
            $message = 'Berhasil tambah data category';
        else :
            $message = 'Gagal tambah data category';
        endif;
        
        $arr = array(
            'message' => $message
        );

        echo json_encode($arr);
    }
    
    public function update_category()
    {
        $valid = $this->form_validation;
        $id    = $this->input->post('id');
        $name  = $this->input->post('name');
   
        $valid->set_rules('name','Nama Cat','required');
        
        $data  = array(
            'id'   => $id,
            'name' => $name,
        );
        
        if ($valid->run()) :
            $sukses = $this->category_model->update_category($data);
        endif;
        
        if ($sukses) :
            $message = 'Berhasil edit data category';
        else :
            $message = 'Gagal edit data category';
        endif;
        
        $arr = array(
            'message' => $message
        );

        echo json_encode($arr);
    }
    
    public function delete_category()
    {
        $id = $this->input->post('id');
        $sukses = $this->category_model->delete_category($id);
        
        if ($sukses) :
            $message = 'Berhasil hapus data category';
        else :
            $message = 'Gagal hapus data category';
        endif;
        
        $arr = array(
            'message' => $message
        );

        echo json_encode($arr);
    }
    
    public function json_list_category()
    {
        $data = $this->category_model->get_list_category();
        $all  = (object)array('id' => '', 'name' => 'All', 'status' => NULL);
        array_unshift($data, $all);
        
        echo json_encode($data);
    }
}
