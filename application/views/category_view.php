<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Jagalab Test</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery-eeasyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery-eeasyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery-eeasyui/demo/demo.css">
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-eeasyui/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-eeasyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery-eeasyui/plugins/datagrid-filter.js"></script>
</head>
<body>
	<h2><?=$title?></h2>
	<p><?=$subtitle?></p>
	<div style="margin:20px 0;"></div>
	<table id="tb_category" class="easyui-datagrid" title="Data Category" style="width:700px;height:400px"
			data-options="
                url:'<?=site_url('category/read_category')?>',
                rownumbers:true,
                method:'post',
                singleSelect:true,
                pagination:true,
                multiSort:false,
                remoteSort:false,
                autoRowHeight:true,
                footer:'#ft',
                remoteFilter:true,
                loadMsg:'proses loading data',
            "
    >
		<thead>
			<tr>
				<th data-options="field:'id',width:80,sortable:true">ID</th>
				<th data-options="field:'name',width:100,sortable:true">Name</th>
			</tr>
		</thead>
	</table>

	<div id="ft" style="padding:2px 5px;">
		<a href="javascript:void(0)" onclick="$('#add_category').dialog('open')" class="easyui-linkbutton" iconCls="icon-add" plain="true">Tambah Category</a>
		<a href="javascript:void(0)" onclick="showeditForm()" class="easyui-linkbutton" iconCls="icon-edit" plain="true">Edit Category</a>
		<a href="javascript:void(0)" onclick="showdeleteForm()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">Hapus Category</a>
	</div>
    
    <div id="add_category" class="easyui-dialog" title="Tambah Data category" data-options="iconCls:'icon-save'" style="width:543px;height:200px;padding:10px" closed="true">
        <div class="easyui-panel" title="" style="width:100%;max-width:500px;padding:30px 60px;">
            <form id="form_category" class="easyui-form" method="post" data-options="novalidate:true">
                <div style="margin-bottom:20px">
                    <input class="easyui-textbox" name="name" style="width:100%" data-options="label:'Nama Cat:',required:true,">
                </div>
            </form>
            <div style="text-align:center;padding:5px 0">
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveForm()" style="width:80px">Submit</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">Clear</a>
            </div>
        </div>    
	</div>
    
    <div id="edit_category" class="easyui-dialog" title="Edit Data category" data-options="iconCls:'icon-save'" style="width:543px;height:200px;padding:5px" closed="true">
        <div class="easyui-panel" title="" style="width:100%;max-width:500px;padding:30px 60px;">
            <form id="edit_form_category" class="easyui-form" method="post" data-options="novalidate:true">
                <input type="hidden" name="id" data-options="label:'ID:'">
                <div style="margin-bottom:20px">
                    <input class="easyui-textbox" name="name" style="width:100%" data-options="label:'Nama Cat:',required:true,">
                </div>
            </form>
            <div style="text-align:center;padding:5px 0">
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="editForm()" style="width:80px">Submit</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">Clear</a>
            </div>
        </div>    
	</div>
    
    <script type="text/javascript">    
        $( document ).ready(function() {
            var dg = $('#tb_category');
            dg.datagrid('enableFilter');
            
            window.saveForm = function(){
                $('#form_category').form('submit',{
                    onSubmit:function(){
                        var form = document.querySelector('[id="form_category"]'); 
                        var name = form.querySelector('[name="name"]').value;
                        
                        $(this).form('enableValidation').form('validate');

                        if (name != ''){
                            $.ajax({
                                url         : "<?=base_url('category/add_category')?>",
                                method      : "POST",
                                cache       : false,
                                dataType    : "json",
                                data        : {
                                        name  : name,
                                },
                                success :function(data){
                                  $.messager.alert('Info',data.message,'info');
                                  $('#form_category').form('clear');
                                  $('#add_category').dialog('close');
                                  $('#tb_category').datagrid('reload');
                                }
                              });
                              
                        } else {
                            return false;
                        }
                    },
                });

            }
            
            window.showeditForm = function(){
                var row = $('#tb_category').datagrid('getSelected');
                
                if (!row) {
                    $.messager.alert('Info','Pilih Data Yang akan diedit terlebih dahulu di datagrid!','warning');
                    return false;
                }
                
                $('#edit_form_category').form('load',row);
                $('#edit_category').dialog('open');
            }
            
            window.editForm = function(){
                        var form        = document.querySelector('[id="edit_form_category"]');    
                        var id          = form.querySelector('[name="id"]').value;
                        var name        = form.querySelector('[name="name"]').value;
                                                
                        $('#edit_form_category').form('enableValidation').form('validate');
                        
                        if (name != ''){
                            
                            $.ajax({
                                url      : "<?=base_url('category/update_category')?>",
                                method   : "POST",
                                cache    : false,
                                dataType : "json",
                                data     : {
                                    id    : id,
                                    name  : name,
                                },
                                success :function(data){
                                  $.messager.alert('Info',data.message,'info');
                                  $('#edit_form_category').form('clear');
                                  $('#edit_category').dialog('close');
                                  $('#tb_category').datagrid('reload');
                                }
                              });
                            
                        } else {
                            return false;
                        }
            }
            
            window.showdeleteForm = function(){
                var row = $('#tb_category').datagrid('getSelected');
                
                if (!row) {
                    $.messager.alert('Info','Pilih Data Yang akan hapus terlebih dahulu di datagrid!','warning');
                    return false;
                }
                
                $.messager.confirm('Confirmation', 'Apakah anda yakin akan menghapus data ini?', function(r){
                    if (r){
                        $.ajax({
                                url : "<?=base_url('category/delete_category')?>",
                                method : "POST",
                                cache : false,
                                dataType: "json",
                                data : {
                                  id : row.id,
                                },
                                success :function(data){
                                  $.messager.alert('Info',data.message,'info');
                                  $('#tb_category').datagrid('reload');
                                }
                        });
                    }
                });
            }
            
            window.clearForm = function(){
                $('#form_category').form('clear');
                $('#edit_form_category').form('clear');
            }
            
        });
    </script>        
</body>
</html>