<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Jagalab Test - Home</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery-eeasyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery-eeasyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery-eeasyui/demo/demo.css">
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-eeasyui/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-eeasyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery-eeasyui/plugins/datagrid-filter.js"></script>
</head>

<body class="easyui-layout">

    <div data-options="region:'north',border:false" style="height:60px;background:#B3DFDA;padding:10px">
        Header
    </div>
	<div data-options="region:'west',split:true,title:'Menu'" style="width:150px;padding:10px;">
        Category <br>
        Product
    </div>
	<div data-options="region:'east',split:true,collapsed:true,title:'East'" style="width:100px;padding:10px;">
        Sidebar
    </div>
	<div data-options="region:'south',border:false" style="height:50px;background:#A9FACD;padding:10px;">
        Footer
    </div>
	<div data-options="region:'center',title:'Content'">
    </div>


</body>
</html>