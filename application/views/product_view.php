<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Jagalab Test</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery-eeasyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery-eeasyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery-eeasyui/demo/demo.css">
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-eeasyui/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-eeasyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery-eeasyui/plugins/datagrid-filter.js"></script>
</head>
<body>
	<h2><?=$title?></h2>
	<p><?=$subtitle?></p>
	<div style="margin:20px 0;"></div>
	<table id="tb_product" class="easyui-datagrid" title="Data Product" style="width:1000px;height:450px"
			data-options="
                url:'<?=site_url('product/read_product')?>',
                rownumbers:true,
                method:'post',
                singleSelect:true,
                pagination:true,
                multiSort:false,
                remoteSort:false,
                remoteFilter:true,
                autoRowHeight:true,
                toolbar:'#tb',
                footer:'#ft',
                loadMsg:'proses loading data',
            ">
		<thead>
			<tr>
				<th data-options="field:'id',width:80,sortable:true">ID</th>
				<th data-options="field:'category_name',width:150,sortable:true">Category</th>
				<th data-options="field:'code',width:80,align:'right',sortable:true">Code</th>
				<th data-options="field:'name',width:200,align:'right',sortable:true">Nama</th>
                <th data-options="field:'date',width:150,align:'right',sortable:true">Date</th>
				<th data-options="field:'description',width:150,align:'right',sortable:true">Deskripsi</th>
				<th data-options="field:'image',width:140,align:'right'" formatter="formatDetail">Image</th>
			</tr>
		</thead>
	</table>
	<div id="ft" style="padding:2px 5px;">
		<a href="javascript:void(0)" onclick="$('#add_product').dialog('open')" class="easyui-linkbutton" iconCls="icon-add" plain="true">Tambah Product</a>
		<a href="javascript:void(0)" onclick="showeditForm()" class="easyui-linkbutton" iconCls="icon-edit" plain="true">Edit Product</a>
		<a href="javascript:void(0)" onclick="showdeleteForm()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">Hapus Product</a>
        <a href="javascript:void(0)" onclick="showDetail()" class="easyui-linkbutton" iconCls="icon-menu" plain="true">Detail</a>
	</div>
    
    <div id="add_product" class="easyui-dialog" title="Tambah Data Product" data-options="iconCls:'icon-save'" style="width:450px;height:450px;padding:10px" closed="true">
        <div class="easyui-panel" title="" style="width:100%;max-width:400px;padding:30px 60px;">
            <form id="form_product" class="easyui-form" method="post" data-options="novalidate:true" enctype="multipart/form-data">
                <div style="margin-bottom:20px">
                    <input class="easyui-textbox" name="code" style="width:100%" data-options="label:'Code:',required:true">
                </div>
                <div style="margin-bottom:20px">
                    <input class="easyui-textbox" name="name" style="width:100%" data-options="label:'Nama:',required:true,">
                </div>
                <div style="margin-bottom:20px">
                    <select class="easyui-combobox" name="category" 
                        data-options="
                            url:'<?php echo site_url('category/json_list_category')?>',
                            label:'Category:',
                            required:true,
                            valueField:'id',
                            textField:'name',
                        "
                    style="width:100%">
                     
                    </select>    
                </div>
                <div style="margin-bottom:20px">
                    <input class="easyui-datebox" name="date" style="width:100%" data-options="label:'Date:',parser:dateParser,formatter:dateFormatter"> 
                </div>
                <div style="margin-bottom:20px">
                    <input class="easyui-textbox" name="description" style="width:100%;height:60px" data-options="label:'Description:',multiline:true">
                </div>
                <div style="margin-bottom:20px">
                    <input class="easyui-filebox" name="image" label="Image:" labelPosition="left" data-options="prompt:'Choose a file...'" style="width:100%">
                </div>
            </form>
            <div style="text-align:center;padding:5px 0">
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveForm()" style="width:80px">Submit</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">Clear</a>
            </div>
        </div>    
	</div>
    
    <div id="edit_product" class="easyui-dialog" title="Edit Data Product" data-options="iconCls:'icon-save'" style="width:450px;height:450px;padding:10px" closed="true">
        <div class="easyui-panel" title="" style="width:100%;max-width:400px;padding:30px 60px;">
            <form id="edit_form_product" class="easyui-form" method="post" data-options="novalidate:true">
                <input type="hidden" name="id" data-options="hidden:true,label:'ID:'">
                <div style="margin-bottom:20px">
                    <input class="easyui-textbox" name="code" style="width:100%" data-options="label:'Code:',required:true">
                </div>
                <div style="margin-bottom:20px">
                    <input class="easyui-textbox" name="name" style="width:100%" data-options="label:'Nama:',required:true,">
                </div>
                <div style="margin-bottom:20px">
                    <select class="easyui-combobox" name="category" 
                        data-options="
                        label:'Category:',
                        required:true,
                        url :'<?php echo site_url('category/json_list_category');?>',
                        valueField:'id',
                        textField:'name',
                    " style="width:100%">
                    </select>
                </div>
                <div style="margin-bottom:20px">
                    <input class="easyui-datebox" name="date" style="width:100%" data-options="label:'Date:',parser:dateParser,formatter:dateFormatter"> 
                </div>
                <div style="margin-bottom:20px">
                    <input class="easyui-textbox" name="description" style="width:100%;height:60px" data-options="label:'Description:',multiline:true">
                </div>
                <div style="margin-bottom:20px">
                    <input class="easyui-filebox" name="image" label="Image:" labelPosition="left" data-options="prompt:'Choose a file...'" style="width:100%">
                </div>
            </form>
            <div style="text-align:center;padding:5px 0">
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="editForm()" style="width:80px">Submit</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">Clear</a>
            </div>
        </div>    
	</div>

    
    <script type="text/javascript">
        function formatDetail(value,row){
            if (row.image != null) {
                var href = '<?=base_url()?>uploads/'+row.image;
                return '<a target="_blank" href="' + href + '">'+row.image+'</a>';
            } else {
                return '';
            }
        }
        
        function dateFormatter(date) {
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            var d = date.getDate();
            if (y == '0000' || m == '0' || d == '0') {

            } else {
                return y + '-' + (m < 10 ? ('0' + m) : m) + '-' + (d < 10 ? ('0' + d) : d);
            }
        }
        
        function dateParser(s) {
            if (!s) return new Date();
            var ss = (s.split('-'));
            var y = parseInt(ss[0], 10);
            var m = parseInt(ss[1], 10);
            var d = parseInt(ss[2], 10);
            if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
                return new Date(y, m - 1, d);
            } else {
                return new Date();
            }
        }
    
        $( document ).ready(function() {
            var dg = $('#tb_product');
            dg.datagrid('enableFilter',[{
                    field: 'category_name',
                    type: 'combobox',
                    options: {
                        editable: false,
                        url :'<?php echo site_url('category/json_list_category');?>',
                        valueField:'id',
                        textField:'name',
                        onChange: function (value) {
                            dg.datagrid('addFilterRule', {
                                field: 'category',
                                op: 'equal',
                                value: value
                            });
                            dg.datagrid('doFilter');
                        }
                    }
                },
                {
                    field: 'date',
                    type: 'datebox',
                    options:{
                        editable: true,
                        formatter:dateFormatter,
                        parser:dateParser,
                        onChange: function (value) {
                            dg.datagrid('addFilterRule', {
                                field: 'date',
                                op: 'equal',
                                value: value
                            });
                            dg.datagrid('doFilter');
                        }
                    }
                },
                {
                    field:'image',
                    type: 'text',
                    options:{
                        editable: false,
                    }
                }
            ]);
            
            window.saveForm = function(){
                $('#form_product').form('submit',{
                    onSubmit:function(){
                        var form      = document.querySelector('[id="form_product"]'); 
                        var formData  = new FormData();
                        
                        formData.append('code', form.querySelector('[name="code"]').value);
                        formData.append('name', form.querySelector('[name="name"]').value);
                        formData.append('category', form.querySelector('[name="category"]').value);
                        formData.append('date', form.querySelector('[name="date"]').value);
                        formData.append('description', form.querySelector('[name="description"]').value);
                        formData.append('image', form.querySelector('[name="image"]').files[0]);
                        
                        $(this).form('enableValidation').form('validate');
                        
                        if (formData.code != '' && formData.name != '' && formData.category != ''){
                            
                            $.ajax({
                                url : "<?=base_url('product/add_product')?>",
                                method : "POST",
                                cache : false,
                                dataType: "json",
                                contentType: false,
                                processData: false,
                                data : formData,
                                success :function(data){
                                  $.messager.alert('Info',data.message,'info');
                                  $('#form_product').form('clear');
                                  $('#add_product').dialog('close');
                                  $('#tb_product').datagrid('reload');
                                }
                              });
                            
                            
                        } else {
                            return false;
                        }
                        
                        
                    },
                });

            }
            
            window.showeditForm = function(){
                var row = $('#tb_product').datagrid('getSelected');
                
                if (!row) {
                    $.messager.alert('Info','Pilih Data Yang akan diedit terlebih dahulu di datagrid!','warning');
                    return false;
                }
                
                $('#edit_form_product').form('load',row);
                $('#edit_product').dialog('open');
            }
            
            
            window.editForm = function(){
                        var form      = document.querySelector('[id="edit_form_product"]');
                        var formData  = new FormData();
                        
                        formData.append('id', form.querySelector('[name="id"]').value);
                        formData.append('code', form.querySelector('[name="code"]').value);
                        formData.append('name', form.querySelector('[name="name"]').value);
                        formData.append('category', form.querySelector('[name="category"]').value);
                        formData.append('date', form.querySelector('[name="date"]').value);
                        formData.append('description', form.querySelector('[name="description"]').value);
                        formData.append('image', form.querySelector('[name="image"]').files[0]);
                                                
                        $('#edit_form_product').form('enableValidation').form('validate');
                        
                        if (formData.code != '' && formData.name != '' && formData.category != ''){
                            
                            $.ajax({
                                url : "<?=base_url('product/update_product')?>",
                                method : "POST",
                                cache : false,
                                dataType: "json",
                                contentType: false,
                                processData: false,
                                data : formData,
                                success :function(data){
                                  $.messager.alert('Info',data.message,'info');
                                  $('#edit_form_product').form('clear');
                                  $('#edit_product').dialog('close');
                                  $('#tb_product').datagrid('reload');
                                }
                              });
                            
                        } else {
                            return false;
                        }
            }
            
            window.showdeleteForm = function(){
                var row = $('#tb_product').datagrid('getSelected');
                
                if (!row) {
                    $.messager.alert('Info','Pilih Data Yang akan hapus terlebih dahulu di datagrid!','warning');
                    return false;
                }
                
                $.messager.confirm('Confirmation', 'Apakah anda yakin akan menghapus data ini?', function(r){
                    if (r){
                        $.ajax({
                                url : "<?=base_url('product/delete_product')?>",
                                method : "POST",
                                cache : false,
                                dataType: "json",
                                data : {
                                  id : row.id,
                                },
                                success :function(data){
                                  $.messager.alert('Info',data.message,'info');
                                  $('#tb_product').datagrid('reload');
                                }
                        });
                    }
                });
            }
            
            window.clearForm = function(){
                $('#form_product').form('clear');
                $('#edit_form_product').form('clear');
            }
            
            window.showDetail = function(){
                var row = $('#tb_product').datagrid('getSelected');
                
                if (!row) {
                    $.messager.alert('Info','Pilih Data Yang akan dilihat terlebih dahulu di datagrid!','warning');
                    return false;
                }
                
                $('#edit_form_product').form('load',row);
                $('#edit_product').dialog('open').setTitle('Detail Product');
            }
        });
	</script>

</body>
</html>