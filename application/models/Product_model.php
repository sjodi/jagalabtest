<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model
{
   public function __construct()
   {
        parent::__construct();
        $this->load->database();
   }
   
   public function list_product()
   {
        $result  = array();
        $page    = !empty($this->input->post('page')) ? intval($this->input->post('page')) : 1;
        $rows    = !empty($this->input->post('rows')) ? intval($this->input->post('rows')) : 10;
        $sort    = !empty($this->input->post('sort')) ? strval($this->input->post('sort')) : 'id';
        $order   = !empty($this->input->post('order')) ? strval($this->input->post('order')) : 'asc';
        $offset  = ($page - 1) * $rows;
        
        $this->db->start_cache();
        
        $this->db->select(array('product.*','category.name as category_name'))
            ->from('product')
            ->join('category','category.id = product.category','left');
        
        if (!empty($this->input->post('filterRules'))){
            $filterData = json_decode($this->input->post('filterRules'));
            
            foreach ($filterData as $item) {
                if ($item->field == 'id') {
                    if (!empty($item->value)){
                        $this->db->where("product.id LIKE '%$item->value%'");
                    }
                }
                else if ($item->field == 'name') {
                    if (!empty($item->value)){
                        $this->db->where("product.name LIKE '%$item->value%'");
                    }
                }
                else if ($item->field == 'code') {
                    if (!empty($item->value)){
                        $this->db->where("product.code LIKE '%$item->value%'");
                    }
                }
                else if ($item->field == 'category') {
                    if (!empty($item->value)){
                        $this->db->where("product.category = '$item->value'");
                    }
                }
                else if ($item->field == 'description') {
                    if (!empty($item->value)){
                        $this->db->where("product.description LIKE '%$item->value%'");
                    }
                }
                else if ($item->field == 'date') {
                    if (!empty($item->value)){
                        $this->db->where("product.date = '$item->value'");
                    }
                } 
                else if ($item->field == 'image') {
                    if (!empty($item->value)){
                        $this->db->where("product.image LIKE '%$item->value%'");
                    }
                } else {
                    //default
                }
            }
        }
        
        $this->db->order_by($sort,$order);
        $this->db->limit($rows,$offset);
        
        $products = $this->db->get()->result_array();
        
        $this->db->stop_cache();
        
        $result['total'] = $this->db->get()->num_rows();
        
        $result = array_merge($result,array('rows'=>$products));
        
        return $result;
   }
   
   public function get_category()
   {
       $query = $this->db->get('category');
       return $query->result_array();
   }
   
   public function insert_product($data)
   {
       return $this->db->insert('product',$data);
   }
   
   public function update_product($data)
   {
        $this->db->where('id', $data['id']);
        return $this->db->update('product',array(
			'code'          =>$data['code'],
			'name'          =>$data['name'],
			'category'      =>$data['category'],
            'description'   =>$data['description'],
            'image'         =>$data['image'],
            'date'          =>$data['date']
        ));
   }
   
   public function delete_product($id)
   {
       $this->db->where('id',$id);
       return $this->db->delete('product');
   }
}