<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model
{
   public function __construct()
   {
        parent::__construct();
        $this->load->database();
   }
   
   public function list_category()
   {
        $result = array();
        $page    = !empty($this->input->post('page')) ? intval($this->input->post('page')) : 1;
        $rows    = !empty($this->input->post('rows')) ? intval($this->input->post('rows')) : 10;
        $sort    = !empty($this->input->post('sort')) ? strval($this->input->post('sort')) : 'id';
        $order   = !empty($this->input->post('order')) ? strval($this->input->post('order')) : 'asc';
        $offset  = ($page - 1) * $rows;
        
        $this->db->start_cache();
        
        $this->db->select('id,name');
        $this->db->from('category');

        if (!empty($this->input->post('filterRules'))) {
           $filterdata = json_decode($this->input->post('filterRules'));
           
            foreach ($filterdata as $item) {
                switch ($item->field) {
                    case "id" :
                        if (!empty($item->value)) {
                            $this->db->where("id LIKE '%$item->value%'");
                        }
                    break;
                    case "name" :
                        if (!empty($item->value)) {
                            $this->db->where("name LIKE '%$item->value%'");
                        }
                    break;
                    default :
                        //else
                    break;
                }
            }
            
        }
       
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $offset);
        $category = $this->db->get()->result_array();
        
        $this->db->stop_cache();
        
        $result['total'] = $this->db->get()->num_rows();
       
        $result = array_merge($result,array('rows'=>$category)); 
        return $result;
   }
   
   public function insert_category($data)
   {
       return $this->db->insert('category',$data);
   }
   
   public function update_category($data)
   {
       $this->db->where('id',$data['id']);
       return $this->db->update('category',array(
            'name' => $data['name']
       ));
   }
   
   public function delete_category($id)
   {
       $this->db->where('id',$id);
       return $this->db->delete('category');
   }
   
   public function get_list_category()
   {
       return $result = $this->db->get('category')->result_array();
       
   }
}